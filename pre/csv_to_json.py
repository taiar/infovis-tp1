#!/usr/bin/env python
# coding: utf-8

import sys
import json
import numpy as np

infile = sys.argv[1]
outfile = sys.argv[2]

csv = open(infile, 'r')
columns = csv.readline().strip().split(',')

metanum = 4
meta = [m.replace(' ', '') for m in columns[:metanum]]
data = columns[metanum:]

series = { 'legend': { 'data': data[:-1] } }
vals = []
for line in csv:
    obj = { 'data': [] }
    fields = line.strip().split(',')
    for i in range(metanum):
        obj[meta[i]] = fields[i]
    for i in range(len(data[:-1])):
        try:
            val = float(fields[i + metanum])
        except:
            print i + metanum, len(fields)
        obj['data'].append(val)
    vals.append(obj['data'])
    obj['sum'] = np.sum(obj['data'])
    obj['mean'] = np.mean(obj['data'])
    obj['median'] = np.median(obj['data'])
    series[fields[1]] = obj

series['limits'] = { 'min': np.min(vals), 'max': np.max(vals) }
jsonfile = open(outfile, 'w')
jsonfile.write('var graph_data = ' + json.dumps(series))

csv.close()
jsonfile.close()
