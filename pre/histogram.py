#!/usr/bin/env python
# coding: utf-8

import sys
import matplotlib.pyplot as plt

infile = sys.argv[1]

csv = open(infile, 'r')
columns = csv.readline().strip().split(',')

metanum = 4
meta = [m.replace(' ', '') for m in columns[:metanum]]
data = columns[metanum:]

vals = []
for line in csv:
    fields = line.strip().split(',')
    for i in range(metanum):
        continue
    for i in range(len(data[:-1])):
        try:
            val = float(fields[i + metanum])
        except:
            print i + metanum, len(fields)
        vals.append(val)

csv.close()

plt.hist(vals, bins=30)
plt.title('Contagem de valores')
plt.xlabel('Valor (em %)')
plt.ylabel('Contagem')
plt.show()
