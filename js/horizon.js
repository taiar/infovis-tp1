function Series(axis, points, label, code) {
  this.axis = axis;
  this.points = points;
  this.label = label;
  this.lines = [];
  this.tip = { remove: function() {} };
  this.markBand = { remove: function() {} };

  this.highlightAt = function(paper, ind) {
    this.markBand.remove();

    var markBandWidth = 8;
    this.markBand = paper.rect(this.pos.x + this.axis.step * ind - markBandWidth / 2, this.pos.y, 
                               markBandWidth, this.axis.size.height + 5);
    this.markBand.attr({ stroke: 0, fill: '#AAAAAA', 'fill-opacity': 0.5 });
  }

  this.tipAt = function(paper, ind) {
    this.tip.remove();
    this.tip = paper.text(this.pos.x - 15, // + this.axis.step * ind,
                          this.pos.y + this.axis.size.height - 6,
                          (new Number(this.points[ind])).toFixed(2) + '%');
  };

  this.draw = function(paper, pos, style) {
    this.lines = [];
    this.pos = pos;
    var lastPoint = {};
    var bands = 4;
    var bandLenght = this.axis.range / bands;
    var mirror_neg = $('#mirror-neg').attr('checked');

    /* draw bands separing the points */
    for (var b = 0; b < bands; b++) {
      var neg_band = b < bands / 2;
      var invert = neg_band && mirror_neg ? true : false;

      /* first point requires an absolute move => M */
      movement = 'M ' + pos.x + 
                 ' ' + (this.axis.toPixels(0, bands, invert) + pos.y);

      /* the other points require an absolute line move => L */
      for (var i = 0; i < this.points.length; i++) {
        pointBand = (this.points[i] - axis.limits.min) / bandLenght;
        pointBand = Math.ceil(pointBand) - 1; 
        neg_pointBand = pointBand < bands / 2;

        /* if it's the right band, draw normally */
        if (pointBand == b) {
          /* drawn value is relative to axis' middle and to the band */
          var pval = this.points[i] - this.axis.ymid;
          /* mirror negatives and adjust to band */
          pval = pval < 0 ? -pval : pval;
          var phase = b < bands / 2 ? (bands / 2 - b + 1) : b;
          pval -= (phase - bands / 2) * bandLenght;

          movement += ' L ' + (i * this.axis.step + pos.x)  + 
                      ' ' + (this.axis.toPixels(pval, bands, invert) + pos.y);
        }
        /* negative band is drawn as zero on positive bands */
        else if (neg_pointBand != neg_band) {
          movement += ' L ' + (i * this.axis.step + pos.x)  + 
                      ' ' + (this.axis.toPixels(0, bands, invert) + pos.y);
        }
        /* if it's on a lower band */
        else if (pointBand < b) {
          if(neg_pointBand)
            movement += ' L ' + (i * this.axis.step + pos.x)  + 
                        ' ' + (this.axis.toPixels(bandLenght, bands, invert) + pos.y);
          else
            movement += ' L ' + (i * this.axis.step + pos.x)  + 
                        ' ' + (this.axis.toPixels(0, bands, invert) + pos.y);
        }
        /* if it's on a higher band, draw on bandLenght */
        else if (pointBand > b) {
          if(neg_pointBand)
            movement += ' L ' + (i * this.axis.step + pos.x)  + 
                        ' ' + (this.axis.toPixels(0, bands, invert) + pos.y);
          else
            movement += ' L ' + (i * this.axis.step + pos.x)  + 
                        ' ' + (this.axis.toPixels(bandLenght, bands, invert) + pos.y);
        }
      }
      /* closes the path over the axis */
      var lastPoint = { x: (this.points.length - 1) * this.axis.step + pos.x,
                        y: (this.axis.toPixels(0, bands, invert) + pos.y) }

      movement += ' L ' + lastPoint.x +  
                 ' ' + lastPoint.y + ' Z';

      var element = paper.path(movement);
      var _style = neg_band ? style.neg : style.pos;
      element.attr(_style);
      this.lines.push(element);
    }

    /* label */
    var labelOffset = 80;
    var labelElement = paper.text(pos.x + this.axis.size.width + labelOffset, 
                                  pos.y + this.axis.size.height - 5, this.label);

    /* country flag */
    paper.image("img/" + code + ".gif", this.pos.x + this.axis.size.width + 5, 
                                        this.pos.y + this.axis.size.height - 12, 16, 11);
  };
}

function Axis(labels, size, limits) {
  this.labels = labels;
  this.size = size;
  this.step = size.width / (labels.length - 1);
  this.limits = limits;
  this.range = (limits.max - limits.min);
  this.ymid = limits.min + this.range / 2;

  /* get the number of the nearest series */
  this.numFromPos = function(x) {
    return Math.floor(x / this.step);
  };

  /* give the value in pixels to be added from origin */
  this.toPixels = function(point, scale, invert) {
    scale = scale ? scale : 1;
    var val = point * scale / this.range * this.size.height;
    if (invert)
      return val;
    else 
      return size.height - val;
  };

  /* draws the axis on the given position */
  this.draw = function(paper, pos, style) {
    /* draw the line */
    var origin = 'M ' + pos.x + ' ' + pos.y;
    var finish = 'l ' + size.width + ' 0'; 
    
    var line = paper.path(origin + ' ' + finish);
    line.attr(style);

    /* draw the labels */
    var xPos = pos.x;
    var labelYOffset = 20;
    var tickSize = 4;
    for(var i = 0; i < labels.length; i++) {
      if (i % 5 == 0) {
        paper.text(xPos, pos.y + labelYOffset, labels[i]);
        var tick = 'M ' + xPos + ' ' + (pos.y - tickSize / 2) + ' v ' + tickSize; 
        paper.path(tick).attr(style);
      }
      xPos += this.step;
    }

  }
}

function HorizonGraph(container, size) {
  this.paper = new Raphael(container, size.width, size.height);
  
  /*
  var yearAxis = new Axis([1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000, 2010, 2020],
                          { width: 500, height: 100 },
                          { min: -100, max: 100 });
  var line = new Series(yearAxis, [-80, -60, -40, -20, 0, 20, 40, 60, 80, 100, 60], 'teste')
  line.draw(this.paper, { x: 50, y: 150 }, 
            { pos : { fill: '#9999CC', 'fill-opacity': 0.5, stroke: null },
              neg : { fill: '#CC9999', 'fill-opacity': 0.5, stroke: null }});
  yearAxis.draw(this.paper, { x: 50, y: 250 }, { stroke: '#555' });

  */

  this.size = size;
  this.style = { pos : { fill: '#9999CC', 'fill-opacity': 0.5, stroke: null },
                 neg : { fill: '#CC9999', 'fill-opacity': 0.5, stroke: null }};

  var data_limits = graph_data['limits'];
  console.log(data_limits);
  var max_abs = Math.abs(data_limits.max) > Math.abs(data_limits.min) ? Math.abs(data_limits.max) : 
                                                                        Math.abs(data_limits.min);
  this.range = { min: -max_abs, max: max_abs };

  this.plot = function(codes) {
    this.lines = []
    this.paper.clear();

    var offset = 50;
    var textOffset = 80;
    var horizonOffset = 5;
    var graphHeight = (this.size.height - 2 * offset) / codes.length - horizonOffset;
    var yiter = offset;

    /* draw axis */
    this.axis = new Axis(graph_data['legend'].data,
                         { width: size.width - 2 * offset - textOffset, height: graphHeight },
                         this.range);
    this.axis.draw(this.paper, { x: offset, y: this.size.height - offset }, { stoke: '#555'});

    /* draw graphs */
    for (var c = 0; c < codes.length; c++) {
        this.lines.push(new Series(this.axis, graph_data[codes[c]].data, graph_data[codes[c]].CountryName, graph_data[codes[c]].CountryCode));
        this.lines[c].draw(this.paper, { x: offset, y: yiter }, this.style);
        yiter += graphHeight + horizonOffset;
    }
    
    /* draw overlay element */
    var self = this;
    var overlayElement = this.paper.rect(offset, offset, 
                                         this.size.width - 2 * offset - textOffset,
                                         this.size.height - 2 * offset);
    overlayElement.attr({ stroke: 0, 'fill-opacity': 0, fill: '#999' });

    this.labelElement = { remove: function() {} };
    overlayElement.mousemove(function(e){
      var yband = self.axis.numFromPos(e.x - offset);
      if(yband < self.axis.labels.length) {
        self.labelElement.remove();
        self.labelElement = this.paper.text(self.lines[0].pos.x + self.axis.step * yband,
                                            self.lines[0].pos.y - 10,
                                            self.axis.labels[yband]);
        for (var c = 0; c < codes.length; c++) {
          self.lines[c].tipAt(self.paper, yband);
          self.lines[c].highlightAt(self.paper, yband);
        }
      }
      overlayElement.toFront();
    });

    overlayElement.mouseout(function(e){
      /* check boundaries before removing */
      if(e.x < offset || e.x > size.width - offset - textOffset ||
         e.y < offset || e.y > offset + graphHeight ) {
        self.labelElement.remove();
        for (var c = 0; c < codes.length; c++) {
          self.lines[c].tip.remove();
          self.lines[c].markBand.remove();
        }
      }
    });

  } 

  plot_data = ["DZA", "AGO", "EGY", "BGD", "NER", "LIE", "NAM", "BGR", "BOL", "GHA", "PAK", "CPV", "JOR", "LBR", "LBY", "MYS", "PRI", "BRA", "PRK", "TZA", "BWA", "KHM", "PRY", "HKG", "SAU", "LBN", "SVN", "BFA", "SVK"];
  this.plot(plot_data);
  //this.plot(['BRA', 'SWE', 'ARG', 'USA']);
  //this.plot(['BRA']);
}
