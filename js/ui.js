/* Selected countries that must be plotted */
var plot_data = [];
var graph = null;

$(document).ready(function(){
  /* Create a new graph */
  var graph = new HorizonGraph('graph-container', { width: $(window).width() - 300,
                                                    height: $(window).height() - 80 });
  
  $('#mirror-neg').click(function() {
    graph.plot(plot_data);
  });
  /* Create slider */
  $('#range-slider').slider({
      range: true,
      min: graph.range.min,
      max: graph.range.max,
      values: [graph.range.min, graph.range.max],
      slide: function(event, ui) {
        graph.range = { min: ui.values[0], max: ui.values[1] };
        graph.plot(plot_data);

        $('#range-info').text(new Number(ui.values[0]).toFixed(2) + '% :' +  
                              new Number(ui.values[1]).toFixed(2) + '%')
      }
  });

  /* Initial range info */
  $('#range-info').text(new Number(graph.range.min).toFixed(2) + '% :' +  
                        new Number(graph.range.max).toFixed(2) + '%')

  /* Add CountyName as text and CountyCode as value for the options of the select */
  $.each(graph_data, function(){
    var o = new Option(this.CountryName, this.CountryCode);
    $("#country-select").append(o);
  });

  /* Return the plot_data array with the CountyCode of each selected country on the select */
  $("#generate-graph").click(function(){
    plot_data = [];
    $("#country-select option:selected").each(function () {
      plot_data.push($(this).val());
    });
    graph.plot(plot_data);
  });

  /* Clear all graphs */
  $("#erase-graph").click(function(){
    plot_data = [];
    graph.plot(plot_data);
  });

  /* Create graphs by country/continent association */
  $(".byContinent").click(function(){
    plot_data = [];
    plot_data = plot_data.concat(continents[$(this).attr("id")]);
    graph.plot(plot_data.sort());
  });

  /* Alpha ordenation */
  $("#ordenaAlpha").click(function(){
    graph.plot(plot_data.sort());
  });

  /* Mean sorting */
  $("#ordenaMedia").click(function(){
    var bla = plot_data.sort(function(a, b) {
      return graph_data[a]['mean'] - graph_data[b]['mean'];
    });
    graph.plot(bla);
  });

  /* Median sorting */
  $("#ordenaMediana").click(function(){
    graph.plot(plot_data.sort(function(a, b) {
      return graph_data[a]['median'] - graph_data[b]['median'];
    }));
  });
});
